# from fabric.api import env
import os
# from fabric.contrib.files import append, exists, sed
from fabric.api import local
import time

REPO_URL = 'https://mnlx:Frenetico1@bitbucket.org/mnlx/dj_pipzt.git'
current_path = os.path.dirname(os.path.realpath(__file__))


def deploy_local():
    _mkdir_log()
    _build_images()
    _django_predeploy_commands()
    _docker_deploy()


def deploy_remote():
    _git_clone(REPO_URL)
    _mkdir_log()
    _build_images()
    _django_predeploy_commands()
    _docker_deploy()

def _build_images():
    local('cd dj_pipzt/django_container && docker build -t monolux/django:v1 .')
    local('cd dj_pipzt/redis_container && docker build -t monolux/redis:v1 .')
    local('docker push monolux/redis:v1')
    local('docker push monolux/django:v1')

def _mkdir_log():
    local('sudo mkdir -p /var/log/pipzt/redis ')
    local('sudo mkdir -p /var/log/pipzt/celery')
    local('sudo mkdir -p /var/log/pipzt/nginx')

    local('sudo mkdir -p /var/data/pipzt/redis')

    local('sudo mkdir -p /etc/pipzt/static')

    local('sudo mkdir -p /etc/pipzt/files')

    local('sudo mkdir - p /var/downloads/selenium')


def _git_clone(repo):
    pass
 #   local(f'git clone {repo}')

def _django_predeploy_commands():

    local('cd dj_pipzt && sudo python3 django_container/manage.py collectstatic --no-input')
    local('docker stack rm pipzt')
    local('docker network prune -f')



def _docker_deploy():
    local('cd dj_pipzt && docker deploy -c docker-compose.yml pipzt')



