# from __future__ import absolute_import, unicode_literals
import re
import zipfile
import os
import smtplib
import email.mime.multipart as MIMEMultipart
import email.mime.text as MIMEText
import email.mime.base as MIMEBase
from email import encoders
from selenium import webdriver
#from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import WebDriverException
import time
from .lib.selenium_tasks import *
from .celery import app
import os
from celery.schedules import crontab
# from django.core.wsgi import get_wsgi_application
# application = get_wsgi_application()
# Above Loads django application to access ORM
import django
django.setup()
from API.models import *



@app.task
def sequencer_cel(*args,**kwargs):
    results_to_send = {}

    sequence_set = Runs.objects.get(pk=kwargs['runs_pk']).sequence_set.values('pk')
    driver = SeleniumRunner()
  #  print(driver)
#    driver = webdriver.Chrome()
#    driver.get('https://sourceforge.net/projects/tomcatplugin/?source=directory')

#    driver.find_element_by_xpath(
#        '//a[@title="Download net.sf.eclipse.tomcat.updatesite-20…zip from SourceForge  - 220.9 kB"]').click()

    

    for sequence in sequence_set:

        action_list = Action.objects.filter(owner_id=sequence['pk']).values('pk','json')
        for action in action_list:

            # Encontra apenas o commando não importa se for duas ou uma palavra
            command = re.search(r'^[A-Z|a-z]+(\s?[A-Z|a-z]+)?', action['json']).group()
            # acha apenas o url ou XPATH
            if action['json'].count('http'):
                url_or_xpath = re.search(r'(http|//|xpath)[^\s]+', action['json']).group()

            elif action['json'].count('Pass'):
                break

            else:
                url_or_xpath = re.search(r'(http|//|xpath).+\]', action['json']).group()

            # Condicional abaixo serve para descobrir o o numero da lista do XPATH
            if url_or_xpath.count('xpath'):
                ELEMNUM = int(re.search(r'\[\d\]', url_or_xpath).group()[1])
                url_or_xpath = re.search(r'(//)[^\ )]',
                                         url_or_xpath).group()  # Remove os acrescimos que o addon acrescenta no browser
            else:
                ELEMNUM = 0
        #    try:
            if command == 'Open Browser':
                driver.get(url_or_xpath)  # nesse caso XPATH é na realidade um URL

            elif command == 'Click Element' or command == 'Click Link':
                # Possivelmente no futuro os dois commandos serão separados
                elem = driver.find_elements_by_xpath(url_or_xpath)[ELEMNUM]
                elem.click()

            elif command == 'Input Text':
                # Já que é dificil fazer um regexp confiável do sendkeys, usei o xpath split abaixo
                KEYSTOSEND = action.split(url_or_xpath)[1]
                # Removendo o espaço branco do inicio
                KEYSTOSEND = KEYSTOSEND[re.search(r'^[^\S]+', KEYSTOSEND).end(): -1]

                elem = driver.find_elements_by_xpath(url_or_xpath)[ELEMNUM]
                elem.send_keys(KEYSTOSEND)

            elif command == 'Download':
                # Possivelmente no futuro os dois commandos serão separados
                elem = driver.find_elements_by_xpath(url_or_xpath)[ELEMNUM]
                elem.click()
                time.sleep(5)

                def zipdir(path, ziph: zipfile.ZipFile):
                    # ziph is zipfile handle
                    for root, dirs, files in os.walk(path):
                        for file in files:
                            if not file.count('.zip'):
                                ziph.write(os.path.join(root, file), file)

                zipf = zipfile.ZipFile('/var/selenium/downloads/temp/downloads.zip', 'w', zipfile.ZIP_DEFLATED)
                zipdir('/var/selenium/downloads', zipf)
                zipf.close()

                fromaddr = "analiseecentry@gmail.com"
                toaddr = "analiseecentry@gmail.com"
                filename = '/var/selenium/downloads/temp/Python.zip'
                sender = 'analiseecentry@gmail.com'
                reciever = 'analiseecentry@gmail.com'

                msg = MIMEMultipart.MIMEMultipart()

                msg['From'] = fromaddr
                msg['To'] = toaddr

                msg['Subject'] = "[Reports]"

                body = "Attached"

                msg.attach(MIMEText.MIMEText(body, 'plain'))
                print(MIMEText.MIMEText(body, 'plain'))

                attachment = open('/var/selenium/downloads/temp/downloads.zip', "rb")

                part = MIMEBase.MIMEBase('application', 'octet-stream')
                part.set_payload(attachment.read())
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment', filename=filename)

                msg.attach(part)

                smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
                smtpObj.starttls()
                smtpObj.login('analiseecentry@gmail.com', 'Huskers1')

                smtpObj.sendmail(sender, reciever, msg.as_string())
                print("Successfully sent email")

        #   action['actionresults_set'] = {'result' : 'SUCCESS'}


            # except (IndexError, WebDriverException) as error:
            #     print(error)
            #     action['actionresults_set'] = {'result' : 'Failure'}
            #     action['actionresults_set'] = {'error_msg' : str( error )}
    driver.quit()


    runs_to_send = {'sequence_set':sequence_set}


@app.task
def dictionary(x, y, **kwargs):
    print(x, y, kwargs)
    return (x, y, kwargs)


@app.task
def test(arg):
    print(arg)


@app.task
def add(x, y,**kwargs):

    with open('/django/tasks/files/add.txt', mode='a') as f:
        f.write('2')
    # print('{0}{1}{2}'.format(x,y,kwargs['be_careful']))
    return x + y


@app.task
def mul(x, y):
    return x * y
