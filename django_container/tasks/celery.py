# from __future__ import absolute_import, unicode_literals
# from math import pi
import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pipzt_api.settings')


app = Celery('events',
#             broker='redis://:Frenetico1@redis:6379/0',
              broker='redis://:Frenetico1@localhost:6379/0',
             include=['tasks.tasks'])
# retrieves conf information from file
# app.config_from_object('events.celeryconfig') Not necessary anymore

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.


app.conf.update(
 #   broker_url='redis://:Frenetico1@redis:6379/0',
  #  result_backend='redis://:Frenetico1@redis:6379/0',
    visibility_timeout= 20,
    task_serializer='json',
    result_serializer='json',
    accept_content=['json'],
    celery_beat_scheduler='django_celery_beat.schedulers:DatabaseScheduler',
    result_expires=2,
    # timezone = 'Europe/Oslo'
    enable_utc=True,
    # task_routes = {
    #     'events.tasks.add': {'queue': 'andre'},
    #     'events.tasks.mul': {'queue': 'felipe'},
    #     },
    # beat_schedule = {
    #     'add-every-30-seconds': {
    #         'task': 'events.tasks.add',
    #         'schedule': 10.0,
    #         'args': (16, 16)
    #     },
    # }

)

if __name__ == '__main__':
    app.start()
