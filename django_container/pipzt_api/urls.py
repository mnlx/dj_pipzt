from django.conf.urls import url, include
from django.contrib import admin
from django.http import HttpResponse

def all_urls(request):


    def show_urls(urllist, depth=0):
        list=[]
        for entry in urllist:
            list.append( entry.regex.pattern)
            if hasattr(entry, 'url_patterns'):
                list = list + show_urls(entry.url_patterns, depth + 1)

        return list

    list_ursl = show_urls(urlpatterns)

    return HttpResponse(list_ursl, content_type='application/json')
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('API.urls')),
    # url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('djoser.urls.authtoken')),
    url(r'^auth-api/', include('rest_framework.urls', namespace='rest_framework')),  # user management path
    url(r'^', all_urls)
]

