import json
import time

from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class RestApiRunsTest(LiveServerTestCase):
    """
    Great stuff
    """

    def setUp(self):
        self.chrome = webdriver.Chrome()

    def tearDown(self):
        self.chrome.quit()

    def test_runs_list_api(self):

        self.chrome.get(self.live_server_url + "/api/runs/list/")
        self.chrome.find_elements_by_xpath('//a[@name="raw-tab"]')[0].click()
        self.chrome.find_elements_by_xpath('//textarea[@name="_content"]')[0].click()
        time.sleep(1)
        self.chrome.find_elements_by_xpath('//textarea[@name="_content"]')[0].send_keys(Keys.CONTROL + "a")
        self.chrome.find_elements_by_xpath('//textarea[@name="_content"]')[0].send_keys(Keys.DELETE )

        self.chrome.find_elements_by_xpath('//textarea[@name="_content"]')[0].send_keys(json.dumps({"sequence_set":[],"owner": 1}))
        time.sleep(1)
        self.chrome.find_elements_by_xpath('//button[@title="Make a POST request on the Runs List resource"]')[1].click()
        body_html = self.chrome.find_element_by_css_selector('body')
        self.assertRegexpMatches(body_html.text,r'owner')
        self.assertIn( "Runs List",self.chrome.title)


