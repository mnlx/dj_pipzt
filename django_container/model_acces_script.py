import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pipz_tester_site.settings")

django.setup()

from API.models import *

instances = Action.objects.all()
print(instances)

sequence_list = SequenceList.objects.get(pk=23)

sequence_set_all = sequence_list.sequence_set.all()
for sequence in sequence_set_all:
    action_list = [action.json for action in sequence.action_set.all()]
print(action_list)
