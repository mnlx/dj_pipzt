import requests

"""
Open Browser    http://www.django-rest-framework.org/api-guide/renderers/    ${BROWSER}
Click Link    //a[@href="#how-the-renderer-is-determined"]
Click Link    //a[@href="#templatehtmlrenderer"]
Click Link    //a[@id="search_modal_show"]
Input Text    //input[@name="q"]    tees
Click Element    //button[@class="btn"]
Click Element    xpath=(//code)[32]
Click Element    xpath=(//li)[98]
Click Element    xpath=(//li)[98]
"""

# Data abaixo bastante importante, contem a estrutura do serializer RunsBase_Serializer. Não deletar

data = {'sequence_set':
            [{'action_set':
                  [{'json': 'Open Browser    http://www.django-rest-framework.org/api-guide/renderers/    ${BROWSER}'},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]'},
                   {'json': 'Input Text    //input[@name="q"]    tees'}],
              'name': 'sequence_1'},
             {'action_set':
                  [{'json': 'Click Element    xpath=(//li)[98]'},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]'},
                   {'json': 'Input Text    //input[@name="q"]    tees'}],
              'name': 'sequence_2'},
             {'action_set':
                  [{'json': 'Click Link    //a[@id="search_modal_show"]'},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]'},
                   {'json': 'Input Text    //input[@name="q"]    tees'}],
              'name': 'sequence_3'},
             {'action_set':
                  [{'json': 'Click Link    //a[@href="#templatehtmlrenderer"]'},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]'},
                   {'json': 'Click Link    //a[@id="search_modal_show"]'}],
              'name': 'sequence_4'},
             ],
        }
sequence = {'action_set':
                [{'json': 'Click Link    //a[@id="search_modal_show"]'},
                 {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]'},
                 {'json': 'Input Text    //input[@name="q"]    tees'}],
            'name': 'sequence_3', 'owner': 20}

r = requests.post('http://127.0.0.1:8000/api/sequence/list/', json=sequence)

print(r.status_code)
print(r.content)
print(r)

# action = {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
#           'sequence_id':20,'sequence_txt':'joke'}
#
# # r = requests.post('http://127.0.0.1:8000/api/action/29/',json=action)
# r = requests.post('http://127.0.0.1:8000/api/action/list/',json=action)
#

print(r)
