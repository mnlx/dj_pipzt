import re

one = r'Open Browser    https://www.google.com.br/    ${BROWSER}'
two = r"Click Link    //a[@href=\"https://www.google.com.br/setprefs?sig=0_RMxjTrmsreW4WTiBZGGblevZZ2Q%3D&hl=en&source=homepage&sa=X&ved=0ahUKEwjr-NHP94PZAhXEjpAKHWLnAWMQ2ZgBCAg\"]"
three = r"Click    //a[@href=\"https://www.google.com.br/setprefs?sig=0_RMxjTrmsreW4WTiBZGGblevZZ2Q%3D&hl=en&source=homepage&sa=X&ved=0ahUKEwjr-NHP94PZAhXEjpAKHWLnAWMQ2ZgBCAg\"]"

comp = re.compile(r'[a-z]+')
r = comp.match(one)

print(r.span)

# Encontra apenas o commando não importa se for duas ou uma palavra
re.search(r'^[A-Z|a-z]+(\s?[A-Z|a-z]+)?', one)
# acha apenas o url
re.search(r'(http|//)[^\s]+', one)
