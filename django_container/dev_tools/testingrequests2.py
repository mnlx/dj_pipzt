import pprint

import requests

"""
Open Browser    http://www.django-rest-framework.org/api-guide/renderers/    ${BROWSER}
Click Link    //a[@href="#how-the-renderer-is-determined"]
Click Link    //a[@href="#templatehtmlrenderer"]
Click Link    //a[@id="search_modal_show"]
Input Text    //input[@name="q"]    tees
Click Element    //button[@class="btn"]
Click Element    xpath=(//code)[32]
Click Element    xpath=(//li)[98]
Click Element    xpath=(//li)[98]
"""

# Data abaixo bastante importante, contem a estrutura do serializer RunsBase_Serializer. Não deletar

data = {'sequenceresults_set':
            [{'actionresults_set':
                  [{'timer': 10,
                    'result': 'Success'}, ],
              'name': 'sequence_1'},

             ],
        'owner': 26
        }

r = requests.post('http://127.0.0.1:8000/api/results/list/', json=data)

print(r.status_code)
print(r.content)
print(r)
pprint.pprint(r.content, indent=2)

# action = {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
#           'sequence_id':20,'sequence_txt':'joke'}
#
# # r = requests.post('http://127.0.0.1:8000/api/action/29/',json=action)
# r = requests.post('http://127.0.0.1:8000/api/action/list/',json=action)
#

print(r)
