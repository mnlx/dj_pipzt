import requests

# curl -X POST http://127.0.0.1:8088/auth/users/create/ --data 'username=djoser&password=djoser'
# {"email": "", "username": "djoser", "id":1}


data_sequence_list = \
    {
        'user_id': 1,
        'pk': 21,
        'sequence_set':
            [{'action_set':
                  [{
                       'json': 'Open BrowserWoek    http://www.django-rest-framework.org/api-guide/renderers/    ${BROWSER}',
                       'action_id': 74},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
                    'action_id': 75},
                   {'json': 'Input Text    //input[@name="q"]    tees',
                    'action_id': 76}],
              'name': 'sequence_1',
              'fails_sum': 99,
              'success_sum': 72,
              'pk': 6,
              'sequence_id': 32,
              'api_status': 'to_delete',
              },
             {'action_set':
                  [{'json': 'Click Element    xpath=(//li)[98]',
                    'action_id': 77},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
                    'action_id': 78},
                   {'json': 'Input Text    //input[@name="q"]    tees',
                    'action_id': 79}],
              'name': 'sequence_2',
              'fails_sum': 99,
              'success_sum': 72, 'sequence_id': 35, 'api_status': 'to_delete',
              },
             {'action_set':
                  [{'json': 'Click Link    //a[@id="search_modal_show"]',
                    'action_id': 80},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
                    'action_id': 81},
                   {'json': 'Input Text    //input[@name="q"]    tees',
                    'action_id': 82}],
              'name': 'sequence_3',
              'fails_sum': 99,
              'success_sum': 72,
              'pk': 7,
              'api_status': 'to_delete',
              'sequence_id': 34,
              },
             {'action_set':
                  [{'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
                    'action_id': 83},
                   {'json': 'Click Link    //a[@href="#templatehtmlrenderer"]',
                    'action_id': 84},
                   {'json': 'Click Link    //a[@id="search_modal_show"]',
                    'action_id': 85}],
              'name': 'sequence_4',
              'fails_sum': 99,
              'success_sum': 72,
              'pk': 8,
              'sequence_id': 33,
              'api_status': 'to_delete',
              },
             ],

    }

# r1 = requests.post('http://127.0.0.1:8000/api/sequence/list/',json=data_sequence_list)
r1 = requests.put('http://127.0.0.1:8000/api/sequence/29/', json=data_sequence_list)

#
# data_user = {'username':'mnlx'
#             ,'password':'Frenetico1'}
#
# r2 = requests.post('http://127.0.0.1:8000/auth/users/create/',json=data_user)

print(r1.content)
