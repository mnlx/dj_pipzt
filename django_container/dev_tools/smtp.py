"""
This call sends an email to one recipient, using a validated sender address
Do not forget to update the sender address used in the sample
"""
from mailjet_rest import Client
import os
print(os.environ)
api_key = os.environ['MJ_APIKEY_PUBLIC']
api_secret = os.environ['MJ_APIKEY_PRIVATE']
mailjet = Client(auth=(api_key, api_secret), version='v3.1')
data = {
  'Messages': [
        {
            "From": {
                "Email": "pilot@mailjet.com",
                "Name": "Mailjet Pilot"
            },
            "To": [
                {
                    "Email": "passenger@mailjet.com",
                    "Name": "Passenger"
                }
            ],
            "HTMLPart": "<ul>{% for rock_band in var:rock_bands %}<li>Title: {{ rock_band.name }}<ul>{% for member in rock_band.members %}<li>Member name: {{ member }}</li>{% endfor %}</ul></li>{% endfor %}</ul>",
            "TemplateLanguage": True,
            "Subject": "Legends Of Rock Just Landed In Your Inbox!",
            "Variables": {
                "rock_bands": [
                    {
                        "name": "The Beatles",
                        "members": [
                            "John Lennon",
                            "Paul McCartney",
                            "George Harrison",
                            "Ringo Starr"
                        ]
                    },
                    {
                        "name": "Led Zeppelin",
                        "members": [
                            "Jimmy Page",
                            "John Bonham",
                            "Robert Plant",
                            "John Paul Jones"
                        ]
                    },
                    {
                        "name": "Nirvana",
                        "members": [
                            "Kurt Cobain",
                            "Krist Novoselic",
                            "Dave Grohl"
                        ]
                    },
                    {
                        "name": "Pink Floyd",
                        "members": [
                            "Roger Waters",
                            "David Gilmour",
                            "Nick Mason",
                            "Richard Wright",
                            "Syd Barrett"
                        ]
                    }
                ]
            }
        }
    ]
}
result = mailjet.send.create(data=data)

print(result.status_code)
print(result.json())
