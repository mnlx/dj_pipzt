from django.http import JsonResponse
from rest_framework import generics,permissions

from API.serializers.serializer_periodictasks import *
from API.serializers.serializer_results import *
from API.serializers.serializers import *


# Abaixo geramos o API view do sequence base

class RunsListView(generics.ListCreateAPIView):
    queryset = Runs.objects.all()
    serializer_class = RunsSerializer

    # permission_classes = (permissions.IsAuthenticated,permissions.DjangoModelPermissions)


class RunsDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Runs.objects.all()
    serializer_class = RunsSerializer
    # permission_classes = (permissions.IsAuthenticated,)


class SequenceView(generics.ListCreateAPIView):
    queryset = Sequence.objects.all()
    serializer_class = SequenceSerializer
    # permission_classes = (permissions.IsAuthenticated,)


class SequenceDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sequence.objects.all()
    serializer_class = SequenceSerializer
    # permission_classes = (permissions.IsAuthenticated,)


class ActionListView(generics.ListCreateAPIView):
    queryset = Action.objects.all()
    serializer_class = ActionSerializer
    # permission_classes = (permissions.IsAuthenticated,)


class ActionDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Action.objects.all()
    serializer_class = ActionSerializer
    # permission_classes = (permissions.IsAuthenticated,)


def last_sequence_pk(request):
    LAST_PK = Runs.objects.last().pk
    return JsonResponse({'pk': LAST_PK})


class UserListView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    # permission_classes = (permissions.IsAuthenticated,)


class RunsResultsDetailsViews(generics.RetrieveUpdateDestroyAPIView):
    queryset = Runs.objects.all()
    serializer_class = RunsResultsSerializer


class RunsResultsListViews(generics.ListCreateAPIView):
    queryset = Runs.objects.all()
    serializer_class = RunsResultsSerializer


class PeriodicTasksListViews(generics.ListCreateAPIView):
    queryset = PeriodicTask.objects.all()
    serializer_class = PeriodicTaskSerializer

class PeriodicTasksDetailsViews(generics.RetrieveUpdateDestroyAPIView):
    queryset = PeriodicTask.objects.all()
    serializer_class = PeriodicTaskSerializer


class UsersRunsListView(generics.ListCreateAPIView):
    # queryset = Runs.objects.all()
    serializer_class = RunsSerializer
    def get_queryset(self):
        pk = self.kwargs['user_id']
        return Runs.objects.filter(owner=pk)
