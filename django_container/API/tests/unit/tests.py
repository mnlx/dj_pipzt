from django.test import TestCase
from django.urls import resolve
import pprint
from API.views import *
import json


class RestApiRunsTest(TestCase):
    """
    Great stuff
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_runs_list_view(self):
        url_resolve = resolve('/api/runs/list/')
        self.assertEqual(type(url_resolve.func), type(RunsListView.as_view()))

    def test_create_user_api_post_also_test_runs_list(self):
        # Creates two users
        self.client.post("http://127.0.0.1:8000/auth/users/create/",
                                    {"username": "monolux", "password": "Frenetico1"})
        response = self.client.post("http://127.0.0.1:8000/auth/users/create/",
                                    {"username": "monux", "password": "Frenetico1"})
        response_dic: dict = json.loads(response.content.decode('utf-8'))
        self.assertEquals({'email': '', 'username': 'monux', 'id': 2}, response_dic)

        #Creates runs object
        response = self.client.post("http://localhost:8000/api/runs/list/", {"owner": 1, "is_active": 0})
        response_dic: dict = json.loads(response.content.decode('utf-8'))

        del response_dic['create_date']
        self.assertEquals({'pk': 1, 'sequence_set': [], 'user_id': None,
                           'details': 'http://testserver/api/sequence/1/',
                           'celery_tasks': {'pk': '1', 'name': 'Task00000001',
                                            'task': '', 'args': '[]', 'kwargs': "{'runs_pk': 1}",
                                            'expires': None, 'enabled': 'False', 'interval':
                                                {'pk': '1', 'every': '10', 'period': 'seconds'},
                                            'crontab': None, 'runs': 1}, 'owner': 1}, response_dic)
