from django.contrib.auth.models import User
from django.db import models
from django_celery_beat.models import PeriodicTask, IntervalSchedule


class Runs(models.Model):
    create_date = models.DateTimeField( auto_now_add=True)
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    user_id = models.IntegerField(null=True)
    celery_tasks = models.OneToOneField(PeriodicTask, null=True)
    is_active = models.BooleanField(default=0)


class SequenceListConfig(models.Model):
    owner = models.ForeignKey(Runs, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=0)
    # is_repeat = models.BooleanField(default=0)


class Sequence(models.Model):
    owner = models.ForeignKey(Runs, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    fails_sum = models.IntegerField(default=0)  # total fails of the sequence
    success_sum = models.IntegerField(default=0)
    last_run = models.IntegerField(default=0)  # If 0: not tested, 1: Failed, 2: Success
    last_run_datetime = models.DateTimeField(null=True)
    # Abaixo são as informações necessárias para o Celery
    is_active = models.BooleanField(default=0)
    api_status = models.CharField(max_length=30, null=True)


class Action(models.Model):
    #    Segue o sequence base. O json será um string normal que será convertido em dic na hora de roda o programa
    owner = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    json = models.CharField(max_length=1000000)
    api_status = models.CharField(max_length=30, null=True)


# Abaixo estão as bases de sequencias e ações que serão lidas pelo robô. Portanto não contêm os resultados


class RunsResults(models.Model):
    owner = models.ForeignKey(Runs, on_delete=models.CASCADE, null=True)
    run_number = models.IntegerField(default=0)
    run_date = models.DateTimeField(null=True, auto_now_add=True)

class SequenceResults(models.Model):
    owner = models.ForeignKey(Sequence, on_delete=models.CASCADE)
    result = models.CharField(max_length=30, null=True)  # either fail or success
    sequence_pk = models.IntegerField(null=True)


class ActionResults(models.Model):
    owner = models.ForeignKey(Action, on_delete=models.CASCADE)
    result = models.CharField(max_length=30, null=True)  # either fail or success
    timer = models.CharField(max_length=30, null=True)
