from rest_framework import serializers

from API.lib.serializer_functions import *


class CronScheduleSerializer(serializers.ModelSerializer):
    pk = serializers.CharField(required=False)
    minute = serializers.CharField(required=False)
    hour = serializers.CharField(required=False)
    day_of_week = serializers.CharField(required=False)
    day_of_month = serializers.CharField(required=False)
    month_of_year = serializers.CharField(required=False)

    class Meta:
        model = CrontabSchedule
        fields = ('pk','minute', 'hour', 'day_of_week', 'day_of_month', 'month_of_year')


class IntervalScheduleSerializer(serializers.ModelSerializer):
    pk = serializers.CharField(required=False)
    every = serializers.CharField(required=False)
    period = serializers.CharField(required=False)
    # runs = serializers.IntegerField(required=False)
    class Meta:
        model = IntervalSchedule
        fields = ('pk','every', 'period')


class PeriodicTaskSerializer(serializers.ModelSerializer):
    pk = serializers.CharField(required=False)
    interval = IntervalScheduleSerializer(required=False, allow_null=True)
    crontab = CronScheduleSerializer(required=False, allow_null=True)
    args = serializers.CharField(required=False)
    name = serializers.CharField(required=False)
    kwargs = serializers.CharField(required=False)
    expires = serializers.DateTimeField(required=False, allow_null=True)
    enabled = serializers.CharField(required=False)
    runs = serializers.PrimaryKeyRelatedField(queryset=Runs.objects.all(), required=False,allow_null=True)
    task = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = PeriodicTask
        fields = ('pk', 'name', 'task', 'args', 'kwargs', 'expires', 'enabled', 'interval','crontab','runs')


    def update(self, instance: PeriodicTask, validated_field: dict):
        periodictasks_pk = validated_field.pop("pk")
        runs = validated_field.pop("runs")

        if validated_field['interval']:
            interval_dic = validated_field.pop("interval")
            IntervalSchedule.objects.filter(pk=interval_dic.pop("pk")).update(**interval_dic)
        PeriodicTask.objects.filter(pk=periodictasks_pk).update(**validated_field)

        # runs = Runs.objects.get(runs_pk)

        runs.celery_tasks = PeriodicTask.objects.get(pk=periodictasks_pk)
        runs.save()
        return PeriodicTask.objects.get(pk=periodictasks_pk)