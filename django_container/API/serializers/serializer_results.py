from rest_framework import serializers

from API.lib.serializer_functions import *


class ActionResultsSetSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    class Meta:
        model = ActionResults
        fields = ('pk','timer', 'owner', 'result' )


class SequenceResultsSetSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    # actionresults_set = ActionResultsSetSerializer(many=True)

    class Meta:
        model = SequenceResults
        fields = ('pk', 'owner')


class RunsResultsSetSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    # sequenceresults_set = SequenceResultsSetSerializer(many=True)

    class Meta:
        model = Runs
        fields = ('pk', 'owner')


#### Exp

class ActionResultsSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    json = serializers.CharField(required=False)
    owner = serializers.PrimaryKeyRelatedField(queryset=Sequence.objects.all(),required=False)
    actionresults_set = ActionResultsSetSerializer(many=True, allow_empty=True, required=False)

    class Meta:
        model = Action
        fields = ('pk', 'json', 'api_status', 'owner','actionresults_set')

class SequenceResultsSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    action_set = ActionResultsSerializer(many=True, allow_empty=True, required=False)
    owner = serializers.PrimaryKeyRelatedField(queryset=Runs.objects.all(),required=False)
    sequenceresults_set = SequenceResultsSetSerializer(many=True, allow_empty=True, required=False)


    class Meta:
        model = Sequence
        fields = ('pk', 'name', 'fails_sum', 'success_sum', 'last_run',
                  'action_set',
                  'api_status', 'owner','sequenceresults_set')


class RunsResultsSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    owner = serializers.PrimaryKeyRelatedField(required=False,queryset=User.objects.all())
    details = serializers.HyperlinkedIdentityField(view_name='api:sequence-details',
                                                   lookup_field='pk')
    runsresults_set = RunsResultsSetSerializer(many=True, allow_empty=True, required=False)
    sequence_set = SequenceResultsSerializer(many=True, required=False)



    class Meta:
        model = Runs
        fields = ('pk',
                  'sequence_set',
                  'create_date',
                  'user_id',
                  'details',
                  'celery_tasks',
                  'owner',
                  'runsresults_set'
                  )

class UserSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField()
    sequencelist_set = RunsResultsSerializer(many=True)

    class Meta:
        model = User
        fields = ('pk', 'username', 'sequencelist_set')

