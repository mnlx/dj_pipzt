from API.serializers.serializer_periodictasks import *


class ActionSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    json = serializers.CharField(required=False)
    owner = serializers.PrimaryKeyRelatedField(queryset=Sequence.objects.all(),required=False)
    class Meta:
        model = Action
        fields = ('pk', 'json', 'owner')

    def create(self, validated_data):
        return createAction(validated_data,
                            filter_data=['sequence_id',
                                          'create_date'])

    def update(self, instance, validated_data):
        return updateAction(instance, validated_data, filter_data=['action_set'])


class SequenceSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    action_set = ActionSerializer(many=True, allow_empty=True, required=False)
    owner = serializers.PrimaryKeyRelatedField(queryset=Runs.objects.all(),required=False)

    class Meta:
        model = Sequence
        fields = ('pk', 'name', 'fails_sum', 'success_sum', 'last_run', 'action_set', 'owner')

    def update(self, instance, validated_data):
        return updateSequence(validated_data,
                              instance,
                              filter_data=['action_set'])

    def create(self, validated_data):
        return createSequence(validated_data, filter_data=['action_set'])


class RunsSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField(required=False)
    owner = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    sequence_set = SequenceSerializer(many=True, required=False)
    celery_tasks = PeriodicTaskSerializer(required=False)
    details = serializers.HyperlinkedIdentityField(view_name='api:sequence-details',
                                                   lookup_field='pk')

    class Meta:
        model = Runs
        fields = ('pk',
                  'sequence_set',
                  'create_date',
                  'user_id',
                  'details',
                  'celery_tasks',
                  'owner',
                  )

    def create(self, validated_data):

        interval = IntervalSchedule.objects.get_or_create(every=10, period="seconds")
        last_periodic_task = PeriodicTask.objects.last()

        if last_periodic_task:
            last_period_id = last_periodic_task.pk
        else:
            last_period_id = 0
        period = PeriodicTask(interval=interval[0], name="Task{:08}".format(last_period_id + 1), enabled=False)
        period.save()
        validated_data['celery_tasks'] = period
        validated_data.pop("sequence_set")
        new_run = Runs(**validated_data)
        new_run.save()
        period.kwargs = str({"runs_pk":new_run.pk})
        period.save()
        return new_run

    def update(self, instance, validated_data):

        for sequence in validated_data['sequence_set']:
            action_set = sequence.pop('action_set')
            sequence_pk = sequence.pop('pk')
            if sequence_pk == 0:  # Nesse caso estamos criando uma sequencia nova com novas actions
                sequence_new = sequence['owner'].sequence_set.create(**sequence)
                for action in action_set:
                    del action['pk']
                    sequence_new.action_set.create(**action)

            elif sequence_pk != 0:  # Aqui estamos fazendo update de uma sequencia, com possíveis novas actions
                Sequence.objects.filter(pk=sequence_pk).update(**sequence)
                for action in action_set:
                    action_pk = action.pop('pk')
                    if action_pk == 0:
                        action['owner'].action_set.create(**action)
                    elif action_pk != 0:
                        Action.objects.filter(pk=action_pk).update(**action)

        validated_data_celery_tasks = validated_data.pop('celery_tasks')

        if validated_data_celery_tasks['crontab']:  # Checks if equals none
            updatePeriodicCronSchedule(instance, validated_data_celery_tasks)
        else:
            updatePeriodicIntervalSchedule(instance, validated_data_celery_tasks)

        return updateRuns(validated_data,
                          instance,
                          filter_data=['sequence_set', 'pk','celery_tasks'])


class UserSerializer(serializers.ModelSerializer):
    pk = serializers.IntegerField()
    runs_set = ActionSerializer(many=True, allow_empty=True, required=False)

    class Meta:
        model = User
        fields = ('pk', 'username', 'runs_set')
