import json
from datetime import datetime, timedelta

from django_celery_beat.models import CrontabSchedule

from API.models import *


def createAction(validated_data, filter_data):
    dic_keys = {x: validated_data[x] for x in validated_data if x not in filter_data}
    sequence = Sequence.objects.get(pk=validated_data['owner'].pk)
    return sequence.action_set.create(**dic_keys)


def updateAction(instance, validated_data, filter_data):
    dic_keys = {x: validated_data[x] for x in validated_data if
                x not in filter_data}
    action, created = Action.objects.get_or_create(validated_data)
    if created:
        return action
    elif created == False:
        Action.objects.filter(pk=instance.id).update(**dic_keys)
        return instance


def createSequence(validated_data, filter_data):
    dic_keys = {x: validated_data[x] for x in validated_data if x not in filter_data}
    sequence_list = Runs.objects.get(pk=validated_data['owner_id'])
    return sequence_list.sequence_set.create(**dic_keys)


def updateSequence(validated_data, instance, filter_data):
    dic_keys = {x: validated_data[x] for x in validated_data if
                x not in filter_data}
    Runs.objects.filter(pk=instance.id).update(**dic_keys)
    return instance


def createRuns(validated_data, filter_data):
    dic_keys = {x: validated_data[x] for x in validated_data if x not in filter_data}
    user = User.objects.get(id=validated_data['owner'].id)
    runs = user.runs_set.create(**dic_keys, create_date=datetime.utcnow())
    count = PeriodicTask.objects.count()
    runs.celery_tasks = PeriodicTask.objects.create(name='sequence{:06}'.format(count+50), enabled=False)
    runs.celery_tasks.interval = IntervalSchedule.objects.get_or_create(period='days', every=10)[0]
    runs.save()
    return runs

def updateRuns(validated_data, instance, filter_data):
    dic_keys = {x: validated_data[x] for x in validated_data if
                x not in filter_data}
    Runs.objects.filter(id=instance.id).update(**dic_keys)
    return instance

def returnActionList(sequencelist_object):
    sequenceset_list = []
    for sequence in sequencelist_object.sequence_set.all():
        actionset_list = [action.json for action in sequence.action_set.all()]
        sequenceset_list += actionset_list
    return [sequenceset_list]

def updatePeriodicCronSchedule(instance, validated_data_celery_tasks):
    periodic_task_object = instance.celery_tasks
    crontab_dic = validated_data_celery_tasks.pop('crontab')
    interval_dic = validated_data_celery_tasks.pop('interval')
    periodic_task_pk = validated_data_celery_tasks.pop('pk')
    validated_data_celery_tasks.pop('runs')

    if not periodic_task_object:  # If the create function hasn't created a periodictasks object
        periodic_task_object = PeriodicTask.objects.create(**validated_data_celery_tasks)
    crontab_dic.pop('pk')
    crontab_obj, _ = CrontabSchedule.objects.get_or_create(**crontab_dic)
    periodic_task_object.crontab = crontab_obj
    if  periodic_task_object.interval:
        periodic_task_object.interval.delete()
        periodic_task_object.interval = None
    PeriodicTask.objects.filter(pk=periodic_task_pk).update(**validated_data_celery_tasks)
    return periodic_task_object.save()

def updatePeriodicIntervalSchedule(instance, validated_data_celery_tasks):
    periodic_task_object = instance.celery_tasks
    crontab_dic = validated_data_celery_tasks.pop('crontab')
    interval_dic = validated_data_celery_tasks.pop('interval')
    periodic_task_pk = validated_data_celery_tasks.pop('pk')
    validated_data_celery_tasks.pop('runs')

    if not periodic_task_object:  # If the create function hasn't created a periodictasks object
        periodic_task_object = PeriodicTask.objects.create(**validated_data_celery_tasks)

    interval_dic.pop('pk')
    periodic_task_object.interval = IntervalSchedule.objects.get_or_create(**interval_dic)[0]
    interval, created = IntervalSchedule.objects.get_or_create(**interval_dic)
    if created:
        periodic_task_object.interval = interval.save()
    else:
        periodic_task_object.interval = interval
    if periodic_task_object.crontab:
        periodic_task_object.crontab.delete()
        periodic_task_object.crontab = None
    PeriodicTask.objects.filter(pk=periodic_task_pk).update(**validated_data_celery_tasks)
    return periodic_task_object.save()
