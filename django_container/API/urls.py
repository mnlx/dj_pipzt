from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from . import views
app_name = 'api'
users_url = [
    url(r'^(?P<user_id>.+)/runs/list$', views.UsersRunsListView.as_view(), name='user-runs'),
    url(r'^list/$', views.UserListView.as_view(), name='user-list'),
]
urlpatterns = [
    # url(r'^user/(?P<username>.+)/$', views.UserListView.as_view(), name='user-list'),
    url(r'^user/', include(users_url)),

    url(r'^runs/list/$', views.RunsListView.as_view(), name='runs-list'),
    url(r'^runs/(?P<pk>[0-9]+)/$', views.RunsDetailsView.as_view(), name='runs-details'),


    url(r'^sequence/list/$', views.SequenceView.as_view(), name='sequence-list'),
    url(r'^sequence/(?P<pk>[0-9]+)/$', views.SequenceDetails.as_view(), name='sequence-details'),

    url(r'^action/list/$', views.ActionListView.as_view(), name='action-list'),
    url(r'^action/(?P<pk>[0-9]+)/$', views.ActionDetails.as_view(), name='action-detail'),

    # url(r'^sequence/last/$', views.last_sequence_pk),  # returns last pk for runs object


    url(r'^results/list/$', views.RunsResultsListViews.as_view(), name='results-list'),
    url(r'^results/(?P<pk>[0-9]+)/$', views.RunsResultsDetailsViews.as_view(), name='results-details'),

    url(r'^tasks/list/$', views.PeriodicTasksListViews.as_view(), name='tasks-list'),
    url(r'^tasks/(?P<pk>[0-9]+)/$', views.PeriodicTasksDetailsViews.as_view(), name='tasks-details'),

]



urlpatterns = format_suffix_patterns(urlpatterns)
